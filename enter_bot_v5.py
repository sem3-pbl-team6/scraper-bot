import requests
from bs4 import BeautifulSoup
import sqlite3
import re
from fuzzywuzzy import fuzz


def match_titles(db_titles, store_title):
    max_percentage, percentage = 0, 0

    # Find words with numbers.
    words_with_numbers = []
    temp = store_title.split()
    for idx in temp:
        if any(chr.isalpha() for chr in idx) and any(chr.isdigit() for chr in idx):
            words_with_numbers.append(idx)

    for db_title in db_titles:
        # Find the percentage of similarity between DB title and store title.
        percentage = fuzz.token_sort_ratio(store_title, db_title)

        for word in words_with_numbers:
            if word in db_title:
                if len(word) >= 5 and percentage > 70:
                    percentage = 100
                elif len(word) < 5:
                    percentage += 5

        # If a title is contained in another one then the match is true.
        if store_title in db_title or db_title in store_title:
            percentage = 100

        # Store the max product.
        if percentage > max_percentage:
            max_percentage = percentage
            max_product = db_title

            # Escape redundant repetitions.
            if max_percentage == 100:
                break

    if max_percentage > 75:
        return max_product
    else:
        return None


def property_id(name):
    c.execute("SELECT PropertyID FROM Properties WHERE Name = ?", (name,))
    return c.fetchone()[0]


def product_id(t, sd):
    if sd:
        c.execute("SELECT ProductID FROM Products WHERE Title = ? AND ShortDescription = ?", (t, sd))
        return c.fetchone()[0]
    else:
        c.execute("SELECT ProductID FROM Products WHERE Title = ?", (t,))
        return c.fetchone()[0]


# Connects to the DB and defines a cursor.
conn = sqlite3.connect('website.db')
c = conn.cursor()

# The categories on the website which are going to be entirely scraped.
categories = {
                "Laptopuri": "https://enter.online/laptopuri",
                "Smartphone-uri": "https://enter.online/telefoane/smartphone-uri"
             }

for category_name, category_link in categories.items():
    # Gets the products' titles.
    c.execute("SELECT Title, ShortDescription FROM Products WHERE Category = ?", (category_name,))
    result = c.fetchall()
    db_list = []
    for row in result:
        db_list.append(row[0] + "*" + row[1])

    last_page, page = False, 1
    while last_page is False:
        # Formulate the right url of the category.
        url = f"{category_link}?page={str(page)}"
        # Scraping the html.
        webpage = requests.get(url)
        # Getting to the next page.
        page += 1

        # Creating a Beautiful Soup object
        soup = BeautifulSoup(webpage.content, "html.parser")

        lost_page = soup.find(text="Nu sunt produse")
        if lost_page is not None:
            last_page = True
        else:
            # Finding all the products and unavailable products.
            products = soup.find_all("div", class_=["grid-item", "uk-box-shadow-hover-medium"])
            un_products = soup.find_all("div", class_=["no-stock"])

            # We introduce the needed data in the corresponding lists.
            for product in products:
                # The title data.
                title = product.find("span", class_="product-title").text

                # The price data.
                price = product.find("span", class_="price")
                if price is None:
                    price = product.find("span", class_="price-new").text
                else:
                    price = price.text
                price = price.replace(" ", "")
                match = re.search(r'\d+', price)
                price = int(match.group(0))

                # The links.
                link = product.find("a", href=True)['href']

                # Gets the image.
                img = product.find("img")['data-src']

                # The presence of the product in stock.
                if product in un_products:
                    availability = "No"
                else:
                    availability = "Yes"

                # Gets the short description below the image.
                short_description = product.find("span", class_="product-descr").text
                short_description = short_description.strip()

                # Updates the database.
                # Updates the product already in the DB, or inserts the new one.
                max_product = match_titles(db_list, title)
                if max_product:
                    # Checks if the Enter_link value exists.
                    middle_index = max_product.index("*")
                    max_title = max_product[:middle_index]
                    max_sd = max_product[middle_index + 1:]

                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Enter_link'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the link.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Enter_link'), link)
                        )

                    # Checks if the Enter_price value exists.
                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Enter_price'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the Enter price.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Enter_price'), price)
                        )
                    else:
                        # Updates the enter price.
                        c.execute(
                            "UPDATE Attachments SET PropertyValue = ? WHERE ProductID = ? AND PropertyID = ?",
                            (price, product_id(max_title, max_sd), property_id('Enter_price'))
                        )

                    # Checks if the Enter_availability value exists.
                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Enter_availability'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the enter availability of the product.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Enter_availability'), availability)
                        )
                    else:
                        # Updates the enter availability.
                        c.execute(
                            "UPDATE Attachments SET PropertyValue = ? WHERE ProductID = ? AND PropertyID = ?",
                            (availability, product_id(max_title, max_sd), property_id('Enter_availability'))
                        )
                else:
                    # Inserts a new product into the database.
                    c.execute(
                        '''INSERT INTO Products (Title, ShortDescription, Image, Category) VALUES(?, ?, ?, ?)''',
                        (title, short_description, img, category_name)
                    )

                    # Inserts the link.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Enter_link'), link)
                    )

                    # Inserts the price.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Enter_price'), price)
                    )

                    # Inserts the availability.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Enter_availability'), availability)
                    )
conn.commit()
conn.close()