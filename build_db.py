import sqlite3

conn = sqlite3.connect('website.db')
cursor = conn.cursor()

cursor.execute('''
CREATE TABLE IF NOT EXISTS Properties(
        PropertyID INTEGER PRIMARY KEY AUTOINCREMENT,
        Name TEXT NOT NULL
    )
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS Products(
        ProductID INTEGER PRIMARY KEY AUTOINCREMENT,
        Title TEXT NOT NULL,
        ShortDescription TEXT,
        Image TEXT,
        Category TEXT NOT NULL
    )
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS Attachments(
        ProductID INTEGER, 
        PropertyID INTEGER,
        PropertyValue TEXT,
        FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
        FOREIGN KEY (PropertyID) REFERENCES Properties(PropertyID)
)
''')

conn.close()