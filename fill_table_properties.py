import sqlite3
import pandas as pd

conn = sqlite3.connect('website.db')
cursor = conn.cursor()

values = [('Darwin_link',), ('Enter_link',), ('Maximum_link',), ('Darwin_price',), ('Enter_price',), ('Maximum_price',), \
    ('Darwin_availability',), ('Enter_availability',), ('Maximum_availability',)]
cursor.executemany('''INSERT INTO Properties (Name) VALUES (?)''', values)

df = pd.read_sql_query("SELECT * FROM Properties", conn)
print(df)

conn.commit()
conn.close()