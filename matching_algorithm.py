from fuzzywuzzy import fuzz


def matching(db_titles, store_titles):
    for db_title in db_titles:
        max_percentage, percentage = 0, 0

        words_with_numbers = []
        temp = db_title.split()
        for idx in temp:
            if any(chr.isalpha() for chr in idx) and any(chr.isdigit() for chr in idx):
                words_with_numbers.append(idx)

        for store_title in store_titles:
            percentage = fuzz.token_sort_ratio(db_title, store_title)

            for word in words_with_numbers:
                if word in store_title:
                    if len(word) >= 2 and percentage > 70:
                        percentage = 100
                    elif len(word) < 2:
                        percentage += 5

            if percentage > max_percentage:
                max_percentage = percentage
                max_product = store_title


        if max_percentage > 75:
            print(db_title)
            print(max_product)
            print(words_with_numbers)
            print(max_percentage, "\n"*2)


db_products = []
store_products = []

matching(db_products, store_products)