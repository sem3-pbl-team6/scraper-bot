import requests
from bs4 import BeautifulSoup
import sqlite3
import re
from fuzzywuzzy import fuzz


def match_titles(db_titles, store_title):
    max_percentage, percentage = 0, 0

    # Find words with numbers.
    words_with_numbers = []
    temp = store_title.split()
    for idx in temp:
        if any(chr.isalpha() for chr in idx) and any(chr.isdigit() for chr in idx):
            words_with_numbers.append(idx)

    for db_title in db_titles:
        # Find the percentage of similarity between DB title and store title.
        percentage = fuzz.token_sort_ratio(store_title, db_title)

        for word in words_with_numbers:
            if word in db_title:
                if len(word) >= 5 and percentage > 70:
                    percentage = 100
                elif len(word) < 5:
                    percentage += 5

        # If a title is contained in another one then the match is true.
        if store_title in db_title or db_title in store_title:
            percentage = 100

        # Store the max product.
        if percentage > max_percentage:
            max_percentage = percentage
            max_product = db_title

            # Escape redundant repetitions.
            if max_percentage == 100:
                break

    if max_percentage > 75:
        return max_product
    else:
        return None


def property_id(name):
    c.execute("SELECT PropertyID FROM Properties WHERE Name = ?", (name,))
    return c.fetchone()[0]


def product_id(t, sd):
    if sd:
        c.execute("SELECT ProductID FROM Products WHERE Title = ? AND ShortDescription = ?", (t, sd))
        return c.fetchone()[0]
    else:
        c.execute("SELECT ProductID FROM Products WHERE Title = ?", (t,))
        return c.fetchone()[0]


# Connects to the DB and defines a cursor.
conn = sqlite3.connect('website.db')
c = conn.cursor()

categories = {
                "Laptopuri": "https://maximum.md/ro/tehnica-computerizata/laptopuri-si-computere/laptopuri/",
                "Smartphone-uri": "https://maximum.md/ro/telefoane-si-gadgeturi/telefoane-si-comunicatii/smartphoneuri/"
             }

for category_name, category_link in categories.items():
    # Gets the products' titles.
    c.execute("SELECT Title, ShortDescription FROM Products WHERE Category = ?", (category_name,))
    result = c.fetchall()
    db_list = []
    for row in result:
        db_list.append(row[0] + "*" + row[1])

    last_page, page = False, 1
    while last_page is False:
        # Formulates the url of the webpage.
        url = f"{category_link}/{str(page)}/"

        # Scrapes the html.
        webpage = requests.get(url)

        # Moves to the next page.
        page += 1

        # Creates a Beautiful Soup object.
        soup = BeautifulSoup(webpage.content, "html.parser")

        # Checks the existence of the next page.
        if soup.find(text="Nu sunt produse"):
            last_page = True
        else:
            # Gets all the products.
            products = soup.find_all("div", class_=["wrap_search_page"])

            # Goes through each product and takes the data.
            for product in products:
                # Gets the title.
                title = product.find("div", class_="product__item__title").text
                title = title.strip()

                # Gets the price.
                price = product.find("div", class_="product__item__price-current").text
                price = price.replace(" ", "")
                match = re.search(r'\d+', price)
                price = int(match.group(0))

                # Gets the product's link.
                link = product.find("a", href=True)['href']
                link = f"https://maximum.md{link}"

                # Gets the image.
                img = product.find("img")["data-src"]

                # Checks the availability.
                if product.find("div", class_="text_price_flex hidden"):
                    availability = 'Yes'
                else:
                    availability = 'No'

                # Scrapes the short description of the product.
                short_description = product.find("div", class_="product-item-description").text
                short_description = short_description.strip()

                # Updates the database.
                # Updates the product already in the DB, or inserts the new one.
                max_product = match_titles(db_list, title)
                if max_product:
                    # Divides the string into title and short description.
                    middle_index = max_product.index("*")
                    max_title = max_product[:middle_index]
                    max_sd = max_product[middle_index + 1:]

                    # Checks if the Maximum_link value exists.
                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Maximum_link'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the link.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Maximum_link'), link)
                        )

                    # Checks if the Maximum_price value exists.
                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Maximum_price'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the Maximum price.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Maximum_price'), price)
                        )
                    else:
                        # Updates the Maximum price.
                        c.execute(
                            "UPDATE Attachments SET PropertyValue = ? WHERE ProductID = ? AND PropertyID = ?",
                            (price, product_id(max_title, max_sd), property_id('Maximum_price'))
                        )

                    # Checks if the Maximum_availability value exists.
                    c.execute(
                        "SELECT EXISTS(SELECT * FROM Attachments WHERE ProductID = ? AND PropertyID = ?)",
                        (product_id(max_title, max_sd), property_id('Maximum_availability'))
                    )
                    if c.fetchone()[0] == 0:
                        # Inserts the Maximum availability of the product.
                        c.execute(
                            '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                            (product_id(max_title, max_sd), property_id('Maximum_availability'), availability)
                        )
                    else:
                        # Updates the Maximum availability.
                        c.execute(
                            "UPDATE Attachments SET PropertyValue = ? WHERE ProductID = ? AND PropertyID = ?",
                            (availability, product_id(max_title, max_sd), property_id('Maximum_availability'))
                        )
                else:
                    # Inserts a new product into the database.
                    c.execute(
                        '''INSERT INTO Products (Title, ShortDescription, Image, Category) VALUES(?, ?, ?, ?)''',
                        (title, short_description, img, category_name)
                    )

                    # Inserts the link.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Maximum_link'), link)
                    )

                    # Inserts the price.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Maximum_price'), price)
                    )

                    # Inserts the availability.
                    c.execute(
                        '''INSERT INTO Attachments (ProductID, PropertyID, PropertyValue) VALUES(?, ?, ?)''',
                        (product_id(title, short_description), property_id('Maximum_availability'), availability)
                    )
conn.commit()
conn.close()