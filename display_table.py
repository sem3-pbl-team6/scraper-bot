import sqlite3
import pandas as pd

conn = sqlite3.connect('website.db')
c = conn.cursor()

pd.options.display.width = None
pd.options.display.max_columns = None
pd.options.display.max_colwidth = None
pd.options.display.max_rows = None
pd.options.display.show_dimensions = False

df = pd.read_sql_query("SELECT * FROM Products", conn)
print(df)
conn.close()